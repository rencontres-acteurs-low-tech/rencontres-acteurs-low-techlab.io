---
layout: page
title: Contact
---

## Contacter l'équipe

Une question ? Envie de vous impliquer ? Laissez-nous un message : [equipe_renalt@protonmail.com](mailto:equipe_renalt@protonmail.com)

## Entre acteurs

À la suite de la première rencontre, un besoin de **garder contact** est né. En plus d'une mailing list, le chat "Mattermost" de l'organisation a été **ouvert à toute personne voulant échanger sur les futurs de la communauté Low Tech francophone et ses projets**.

[Rejoindre le chat Mattermost](https://framateam.org/signup_user_complete/?id=zrphewze93grprchrixsgyi8iy) pour travailler ensemble, trouver des contacts, ...

[Rejoindre la mailing list](https://framalistes.org/sympa/subscribe/acteur.rices_lowtech) pour les annonces utiles à tou·tes (prochaines RenALT, événement majeur, ...)