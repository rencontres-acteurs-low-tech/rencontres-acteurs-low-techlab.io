---
layout: page
title: Informations pratiques
---

*Note : les dates et le lieu de la prochaine rencontre ne sont pas encore défini*

<!-- ## 📅 Dates

__*Du 16 au 18 octobre 2020*__

## 🏰 Lieu

__*Autre Soie*__

[_24, Rue Alfred de Musset_](https://www.openstreetmap.org/note/2354457)

_69100 Villeurbanne/Lyon_

**Accès:**
Métro A - 
Tram T3
// Station : Vaulx-en-Velin – La Soie

## ✍️ Inscription

Lien [HelloAsso](https://www.helloasso.com/associations/l-atelier-du-zephyr/evenements/renalt-rencontres-des-acteurs-de-la-low-tech) d'inscription pour la participation aux frais (lieu d'accueil ainsi que les repas matins, midis et soirs).

Places limitées pour cause sanitaire 😷, possibilité de suivre certains ateliers à distance (Voir [programme](/programme)), merci de vous inscrire pour le distanciel sur la page [HelloAsso](https://www.helloasso.com/associations/l-atelier-du-zephyr/evenements/renalt-rencontres-des-acteurs-de-la-low-tech).

## 📋 Programme

Voir [programme](/programme) -->


Envie de participer à l'organisation de cet événement ? Contactez-nous directement à l'adresse suivante: [equipe_renalt@protonmail.com](mailto:equipe_renalt@protonmail.com).
