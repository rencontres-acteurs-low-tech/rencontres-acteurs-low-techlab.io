---
layout: page
title: Programme
---

## Résumé de la première édition

Dorian et Mewen ont présenté la première RenALT à celles et ceux qui n'ont pas pu y être.

**Revoir la présentation** : *à venir, montage en cours*

## Revivez la Rencontre en podcast ou carnets de notes

Conscients du nombre important d'absents et d'absentes, un effort particulier a été mis en place pour documenter la première rencontre.

Vous pouvez d'ores et déjà [**réécouter les enregistrements audios et lire les comptes-rendus ici 🎙**](https://pad.lescommuns.org/renalt_2020?view)

(La mise au propre des notes n'est pas encore tout à fait terminée mais le principal est en ligne)

## Demandez le programme

Les liens font référence à la prise de notes à la volée lors des ateliers et échanges.

### Vendredi

- 🥐 8h45-9h30 : Temps d’arrivée 
- 9h30-9h45 : Mot d’accueil 
- 9h45-10h45 : [Activités pour faire connaissance](https://pad.lescommuns.org/renalt_2020_vendredi?view#1---Activit%C3%A9s-pour-faire-connaissance)
- ☕ 10h45-11h15 : Invitation à remplir l'annuaire/la bulloterie + Pause 
- 11h15-12h15 : [Débat boule de neige « Qu’attendons-nous de ce WE ? »](https://pad.lescommuns.org/renalt_2020_vendredi?view#2-D%C3%A9bat-boule-de-neige-%C2%AB-Qu%E2%80%99attendons-nous-de-ce-WE--%C2%BB)
- 🥗 12h15-14h : Pause repas
- 14h-16h00 : [Causeries sur les enjeux du mouvement low-tech](https://pad.lescommuns.org/renalt_2020_vendredi?view#3---Causeries-sur-les-enjeux-du-mouvement-low-tech)
- 14h: Série 1 d'ateliers
  - [Les communs pour l'écosystème LowTech](https://pad.lescommuns.org/renalt_2020_vendredi?view#31---Les-communs-pour-l%E2%80%99%C3%A9cosyst%C3%A8me-LowTech)
  - [La notion de "un job" à la lumière des Low Tech](https://pad.lescommuns.org/renalt_2020_vendredi?view#32---Un-job-%C3%A0-la-lumi%C3%A8re-des-Low-Tech)
  - [Les Low Tech dans la vraie vie: utilisations et applications](https://pad.lescommuns.org/renalt_2020_vendredi?view#33---Les-Low-Tech-dans-la-vraie-vie-utilisations-et-applications)
- 14h50: Série 2 d'ateliers
  - [Low Tech: définition, valeurs et éthique: I](https://pad.lescommuns.org/renalt_2020_vendredi?view#34---Low-Tech-d%C3%A9finition-valeurs-et-%C3%A9thique)
  - [Pourquoi et comment fédérer une voix politique pour porter la Low Tech](https://pad.lescommuns.org/renalt_2020_vendredi?view#35---Pourquoi-et-comment-f%C3%A9d%C3%A9rer-une-voix-politique-pour-porter-la-Low-Tech)
  - [ESS: Liens entre Low  Tech et la solidarité, le social](https://pad.lescommuns.org/renalt_2020_vendredi?view#36---ESS-Liens-entre-Low-Tech-et-la-solidarit%C3%A9-le-social)
- 15h35: Série 3 d'ateliers
  - [Les communs pour l'écosystème LowTech II](https://pad.lescommuns.org/renalt_2020_vendredi?view#31---Les-communs-pour-l%E2%80%99%C3%A9cosyst%C3%A8me-LowTech)  
  - [Enseignement et Low Tech](https://pad.lescommuns.org/renalt_2020_vendredi?view#37---Enseignement-et-Low-Tech)
- ☕ 16h00-16h30 : Pause
- 16h30-18h15 : [Tour de parole : Faut-il créer une fédération ?](https://pad.lescommuns.org/renalt_2020_vendredi?view#4---Faut-il-cr%C3%A9er-une-f%C3%A9d%C3%A9ration-une-voix-de-la-Low-Tech-)
- 🍝 19h : Repas + Moment convivial

### Samedi

- 🥐 8h-9h: Petit déj'
- 9h-10h : [Atelier créatif "Souvenirs du futur"](https://pad.lescommuns.org/renalt_2020_samedi?view#1---Atelier-cr%C3%A9atif-%E2%80%9CSouvenirs-du-futur%E2%80%9D)
- ☕ 10h-10h30 : Pause
- 10h30-12h15 : [Atelier créatif "Souvenirs du futur" - suite](https://pad.lescommuns.org/renalt_2020_samedi?view#13---Mise-en-commun-par-groupe-de-4-6-et-cr%C3%A9ation-d%E2%80%99un-%E2%80%9Csouvenir-du-futur%E2%80%9D-commun)
- 🥗 12h15-14h : Repas
- 14h-14h45 : Identification des axes de travail
- 14h45-16h : Groupes de travail sur les différents axes
	- [GT Les Communs](https://pad.lescommuns.org/renalt_2020_samedi#22---GT-Les-Communs)
	- [GT Tester / valider les low-tech](https://pad.lescommuns.org/renalt_2020_samedi#23---GT-Tester--valider-les-low-tech)
	- [Atelier Documenter et partager](https://pad.lescommuns.org/renalt_2020_samedi#24---Atelier-Documenter-et-partager)
	- [Les LT pour/dans l’enseignement, la formation, l’animation](https://pad.lescommuns.org/renalt_2020_samedi#25---Les-LT-pourdans-l%E2%80%99enseignement-la-formation-l%E2%80%99animation)
	- [Créer du lien - entre nous](https://pad.lescommuns.org/renalt_2020_samedi#26---Cr%C3%A9er-du-lien---entre-nous)
	- [Diffuser ensemble nos valeurs sans brimer nos libertés](https://pad.lescommuns.org/renalt_2020_samedi?view#27---Diffuser-ensemble-nos-valeurs-sans-brimer-nos-libert%C3%A9s)
- ☕ 16h-16h30 : Pause
- 16h30-18h : [Restitution et Discussions libres](https://pad.lescommuns.org/renalt_2020_samedi?view#2---Axes-de-travail)
- 🍝 19h : Repas + Moment convivial + DJ

### Dimanche matin

- 🥐 8h-9h: Petit déj'  
- 9h-11h : [Assemblée générale (décisions à prendre pour la suite)](https://pad.lescommuns.org/renalt_2020_dimanche?view#1---Assembl%C3%A9e-g%C3%A9n%C3%A9rale-d%C3%A9cisions-%C3%A0-prendre-pour-la-suite)
- ☕ 11h-11h30 : Pause
- 11h30-13h : Tour de ressentis
- 🥗 13h-14h30 : [Le dernier repas](https://pad.lescommuns.org/renalt_2020_dimanche?view#2---Atterissage)
- 14h30-… : Ménage/rangement et aurevoirs


## Résultats du sondage réalisé en amont

En juin 2020, l'équipe d'organisation de la RenALT a réalisé un sondage pour comprendre les attentes des acteurs intéressés. Voilà [les résultats du sondage](/sondage).