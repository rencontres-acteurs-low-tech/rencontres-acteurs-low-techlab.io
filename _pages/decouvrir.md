---
layout: page
title: Découvrir le projet
---

## Un peu d'histoire...

Depuis le premier week-end des communautés low-tech organisé par le [Low-Tech Lab](https://lowtechlab.org/) en septembre 2019, l'idée de faire naître un **réseau structuré rassemblant l'ensemble des acteurs de la Low-Tech** a fait son chemin.

*« Oula oula, pas si vite... euh... la quoi ? »*

La Low-Tech, pardi ! Aussi connues sous le noms de technologies douces, la Low-Tech rassemble toutes ces **technologies utiles, libres, accessibles et durables**, conçues de manière suffisamment simple pour que tout le monde puisse se les approprier, les fabriquer (même à partir de déchets !) et les réparer. Poêle Dragon, cuiseur solaire, douche en cycle fermée, frigo du désert, et j'en passe !

Il est vrai qu'aujourd'hui, si la thématique low-tech prend assurément son envol dans le monde, et plus particulièrement en France, à la faveur d'une médiatisation grandissante, rien ne fédère encore toutes les énergies qui la portent. Des **communautés naissent** en France, en Belgique et au Québec, ici et là des aventuriers de la Low-Tech se lancent dans d'ambitieux projets de tour du monde, une voiture low-tech traverse même actuellement l'Afrique... Bref, sur la planète Low-Tech, ça foisonne, ça grouille, ça fourmille. C'est une petite révolution qui se prépare... **Imaginez si tout ce petit monde se parlait !**

Associations de sensibilisation, Makerspaces, bureaux d'étude low-tech, explorateurs-aventuriers, experts et consultants, porteurs de projets, fonds de dotation, médias, incubateurs low-tech, coopératives... Imaginez un peu tous ces acteurs potentiels, dans toutes leurs diversités et spécificités, partageant pour autant les mêmes valeurs (partage, sobriété heureuse, accessibilité, etc.) et la même vision, celle d'un monde plus durable et plus respectueux des êtres humains et de la planète !

## La Rencontre des Acteurs de la Low-Tech, quésaco ?

Un moment d'échanges et de partage, ouverts à toutes celles et ceux qui de près ou de loin font la Low-Tech, afin de dessiner ensemble les contours de ce que serait, idéalement, cette fameuse **Fédération des Acteurs de la Low-Tech**. 

Répartie sur trois jours, cette rencontre aura 3 objectifs principaux. Le premier se trouve dans le mot **rencontre**, il s'agit pour nous tous et toutes de se retrouver le temps de ce weekend dans un cadre convivial pour apprendre à mieux se connaître et pouvoir dégager des valeurs communes. Le second objectif sera d'énumérer les **besoins et ressources** des acteurs de l'écosystème Low Tech francophone et d'entrevoir les connexions et liens à faire. Pour enfin **se mettre en action**, qui sera l'objectif final de cette rencontre. Concrètement ça veut dire de nouvelles dates de rencontres (locales?), la création de projets répondant à des besoins, des groupes de travail ou encore de pistes de gouvernances partagées pour une éventuelle fédération ou réseau.

## Qu'est-ce que ça donne ?

**Les 16, 17 et 18 octobre 2020 a eu lieu la première rencontre** : 23 structures, une trentaine de participant·es et 1 lieu (L'Autre Soie à Villeubane).

L'intention de ce premier événement :

> **Se connaître** (actions, besoins, attentes) et **poser les bases** d’une **communauté conviviale, ouverte, outillée,** **rassemblée** autours de **valeurs communes** ayant identifié les **directions** et **prochains pas** d’**action collective**

Une réussite ! [Retours sur cette première rencontre](/programme)