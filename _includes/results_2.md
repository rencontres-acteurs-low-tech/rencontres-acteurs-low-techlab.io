En plus de ces lieux, 20% des répondant.e.s n'ont pas spécifié de préférence. Et il a été spécifié que oui la ville, mais ne pas oublier la campagne! Il faut préciser que certaines villes pré-proposées dans la liste ne l'étaient que dû à la possibilité de lieux d'accueil.

#### Autres propositions de lieux d'accueil parmis les répondants

- Deux friches à Paris, une à Montreuil et une dans le 13eme.
- Celle Lévescault (région de Poitiers, lieu des SALT 2019 et 2020)
- Nantes
- Wettolsheim
- Mens (Trièves à 1h de Grenoble)


### Quand et combien de temps ? 📆

![svg](/assets/images/result_files/result_29_1.svg)

![svg](/assets/images/result_files/result_30_1.svg)


Se dégage ici clairement l'idée d'une rencontre le temps d'un week-end du vendredi au samedi.

### Hébergement ⛺

#### Camper ?

![svg](/assets/images/result_files/result_33_0.svg)

#### Payer son hébergement ?

![svg](/assets/images/result_files/result_35_0.svg)

Autres contraintes/limites partagées:
- Nombre d'acteurs suffisant (>= ~15-10)
- Distance par rapport à sa localisation (~2-5h de trajet max).
- La facilité d'accès en transport en commun. 
- Une destination centrale dans l'idéal.
- Pouvoir camper en fourgon amménagé.
- Un coût total: l’alimentation, le logement et transport. Budget limité.


### Une rencontre réussie 🏅

Quelle sont vos attentes ou vos craintes vis-à-vis d'une telle rencontre ? Quels seraient les critères de réussite ? 

#### Attentes
- Poser les premières pierres d'un réseau capable de porter la voix de ses membres, un ambassadeur fort.
- Un programme bien ficelé et des débats bien menés =)
- Co-construire une vision  et avoir une communauté fédératrice autour du sujet.   
- Cadrer les temps de parole pour réussir à être efficace à beaucoup.
- Communiquer sur des normes sanitaires communes & strictes pour éviter tout risque de propagation de la COVID et rassurer ceux qui seraient réticents. 
- Rencontrer la communauté, partager ensemble les initiatives de chacun, et échanger sur les sujets évoqués dans ce questionnaire. 
- Il faut rendre la low tech sexy auprès du grand public et développer des outils en ce sens.
- Rencontres d'autres acteurs de la Low tech.
- ce sera forcément réussi car tout le réseau Français est chaud comme la braise :)
- Partir d’échanges collectifs pour arriver à une vision commune et des actions concrètes 
- Connection à d'autres porteurs.ses de projets, 
- Apprentissage interactif
- Temps libres pour échanger de manière informelle.
- Organisation préalable, animation efficace. 
- Créer et structurer les acteurs de la low-tech pour trouver les complémentarités.
- Rencontrer des nouveau.elles acteur.ices de cet écosystème et éventuellement créer des syngeries avec elleux
- Faire sortir de nouvelles idées pour faire progresser cette sphère Low Tech et commencer à la fédérer afin qu'elle prenne un place plus conséquente dans la société actuelle à de nombreux égards.


#### Critères de réussite 
- Repartir riche de connaissances et d'énergie pour continuer à insuffler dans la pratique de nouvelles manière de co-créer dans la low tech.
- Créer un groupe (asso, ou informel mais qui adhère à des valeurs communes) 
- Identifier clairement les interlocuteurs 
- Avoir un calendrier d'action 
- Caler une date pour une prochaine rencontre 
- Créer un émulation autour d'un projet commun (recherche de financement, création d'un tiers lieu, création d'un média, création d'un portail pour les utilisateurs de low-tech...)
- Faire se rapprocher les acteurs de la low tech, avoir des accomplissements clair à la fin de la rencontre
- Avoir 1 personne dédiée à l animation de notre communauté
- Convivialité de l’événement et la capacité à pouvoir échanger en profondeur sur ce sujet de façon conviviale.
- Ce sera forcément réussi car tout le réseau Français est chaud comme la braise :)
- Avoir une gouvernance pour faire remonter les propositions et décider avec des outils de prises de décisions pertinents pour que chacun soit acteur et actif



#### Craintes
- Temps pour discuter organisation et fonctionnement trop élevé.
- Ne pas réussir à prioriser les questions à aborder lors de cette première rencontre.
- Ne pas réussir à prendre des décisions ou mettre en place des actions/prochains pas.
- Une énième rencontre où nous parlons de la définition générale des low-tech.
- Profiles issus d'un même milieu socio-professionnel selon le lieu et le format choisi pour la rencontre.
- Rassembler une majorité d'acteurs qui font et organisent "avec la tête" car celles et ceux qui font "avec leurs mains" sont généralement trop pris pour être disponible.


### Some love 💞

- Vous allez avoir besoin d'énergie pour mener à bien le projet de fédérer des acteurs venant d'horizons aussi diverses. Bon courage.
- Belle initiative. 
- Des bisoux pour l'initiative
- Merci pour votre taff ! J’espere vraiment arriver sur un truc concret qui ne soit pas chronophage pour mutualiser les infos et partager les avancées !
- Je suis super contente qu'un mouvement s'oriente vers la construction d'un réseau. Je trouve qu'on s'y perd vite entre les milliards d'acteurs proposant des trucs supers mais chacun de son côté (forum, conférence etc..). Donc si nous pouvions tous faire partie (en gardant notre originalité propre) d'une fédération cela pourrait avoir du poids auprès des professionnels et institutions.  Merci pour l'initiative ! 
- Merci pour ce temps passé à entreprendre la démarche de faire rencontrer de belles personnes ayant une vision similaire et désirant contribuer à sa propre échelle :)
- Bisous bisous et tous mes encouragements !
- J'ai hâte de voir la forme que les Rencontres prendront, pour cette première qui ne manquera pas d'être nourrie d'échanges je l'espère. 
- Super beau ce sondage <3 
- Merci pour tout !!
