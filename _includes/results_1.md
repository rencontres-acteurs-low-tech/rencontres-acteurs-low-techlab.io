
# Résultats du Sondage  🎉 

Voici les premiers résultats temporaires du sondage visant à la préparation d'une rencontre des Acteurs de la Low-Tech. Nous avons eu pour l'instant **25 réponses**, comprenant des individus et des organisations. Si ce n'est pas encore fait n'hésitez pas à [le compléter](https://framaforms.org/sondage-renalt-1591864990)!

## Qu(i) êtes-vous ? 👀

![svg](/assets/images/result_files/result_5_0.svg)

Le nombre relativement important d'individus s'explique entre autre par la volonté de certains de s'exprimer en leur nom, plutôt que pour les organisations dont ils sont membres.

### De manière générale, que pensez-vous apporter à "l'écosystème low-tech" ?

![svg](/assets/images/result_files/result_8_0.svg)

On a clairement ici une forte représentation du domaine de la recherche et de la formation. Il serait sans doute utile de voir comment mobiliser d'autres types d'acteurs. Parmis les réponses pour "Autres" on retrouve l'éducation populaire, l'internationnal, l'autoconstruction ou encore le partage de la philosophie.

### Connaissance de l'écosystème 

(0=pas du tout à 4=très bien) 

![svg](/assets/images/result_files/result_11_0.svg)

Parmi les répondants, un nombre important semble déjà avoir un bon aperçu sur la dynamique des Low-Tech en France/francophonie.

### Apports possibles des participant.e.s durant la rencontre

Voici une liste non-exhaustive de propositions d'apports durant la rencontre:

- Conférence sur la low-tech dans la conception et dans la construction d'habitation.
- Réflexions sur le thème de travailler/entreprendre autours de la Low Tech.
- Présentation du Guide de l'Autonomie rassemblant plusieures assos Low-Tech.
- Low-tech pour la solidarité internationale.
- Co-Conception d'ateliers.
- Comment confronter le discours Low-tech face aux entreprises.
- Lien entre milieu académique.
- Retours d'expérience en milieu rural en lien avec le tissu artisanal local.
- Réflexion sur la documentation et la diffusion sous licence libre.
- Méthodes de coordination et de développement de communautés.
- Enthousiasme et idées =)

## Thématique 📚

Voici les intérêts portés sur des sujets d'échanges pour cette première rencontre. (0=pas intéressé.e.s à 4=très intéressé.e.s)


### Low-tech 

![svg](/assets/images/result_files/result_15_0.svg)

### Acteurs low-tech

![svg](/assets/images/result_files/result_17_0.svg)

### Future fédération 

![svg](/assets/images/result_files/result_19_0.svg)

Voilà qui donne un premier aperçu des différents intérêts. Voici également les propositions suppplémentaires soumises:

- Sensibilisation à la Low-Tech ?
- Discussion autour des verrous/limites des low-tech :
  - Verrou législatif (comment faire pour que les low-tech répondent aux normes en place / changer les normes en place / ...) ?
  - La low-tech face aux effets : rebonds, d'échelle et de parc

- Démocratisation des savoirs de la low tech.
- Autonomie et résilience (retour sur le COVID).
- Etat de l'art international des modèles économiques de la lowtech ou proche de la lowtech.
- Low-tech petit budget (par récupération/ up-cycling de déchets).
- Solidarité internationale.
- Low-tech dans politiques territoriales.
- Thématique des artisants ou micro-industries renouvelables et circulaires locales.
- Inclusion aux démarches environnementales.
- Collaborer avec d'autres associations pour mettre en place des évènements communs.

## Infos pratiques

### Villes/lieux 🌍 

Dans quelles villes seriez-vous prêt.e.s à vous rendre pour cette rencontre ?

- Paris: 13
- Lyon: 11
- Bordeaux: 8
- Grenoble: 8
- Poitiers: 7
- Rouen: 4
- Je viendrai/Nous viendrons quoiqu'il en soit!: 4
- Autre(s): 3
- Bretagne/Nantes: 3
- Troyes: 2
- Tours: 1
- Alsace/Wettolsheim: 1
- Pas d'avis: 1
- Marseille: 1
- Auvergne: 1
- Montpellier: 1
- Toulouse: 1