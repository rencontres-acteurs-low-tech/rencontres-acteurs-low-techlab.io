---
layout: page
title: Opensource
---

**Listes des projets opensource utilisés par l'organisation de la rencontre**

Il nous semble important de rendre honneur aux outils qui nous permettent de travailler plus efficacement et qui sont développés sous licences opensources et/ou libres. Voici donc une liste, non exaustive, d'outils que nous utilisons:

- La prise de note: [Framapad](https://framapad.org/) utilisant lui-même [Etherpad](https://github.com/ether/etherpad-lite)
- Les tableurs: [Framacalc](https://framacalc.org/) utilisant lui-même [Ethercalc](https://github.com/audreyt/ethercalc)
- La prise de rendez-vous: [Framadate](https://framadate.org/)
- Les formulaires: [Framaform](https://framaforms.org/) utilisant le [webform](https://www.drupal.org/project/webform) de Drupal
- Les visioconférences: [jitsi](https://jitsi.org/)
- Le chat interne: [Signal](https://signal.org/)
- Les e-mails: [Portonmail](https://protonmail.com/)
- L'hébergement du siteweb: [Gitlab](https://gitlab.com/)
- Le site internet: [Jekyll](https://jekyllrb.com)
- La conversion d'image: [ImageMagick](www.imagemagick.org)
- L'analyse des réponses du sondage: [Jupyter](https://jupyter.org)
- Le code: [Visual Studio Code](https://code.visualstudio.com)
- La conversion de document: [Pandoc](https://pandoc.org)

Et on en oublie certainement beaucoup d'autres!